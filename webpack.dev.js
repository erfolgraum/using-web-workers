const path = require("path");
const { merge } = require("webpack-merge");
const Common = require("./webpack.common.js");

module.exports = merge(Common, {
  mode: "development",

  devServer: {
    static: {
      directory: path.join(__dirname, './src'),
    },
    proxy: {
      "/api": {
        target: "http://localhost:3000",
        pathRewrite: { "^/api": "" },
      },
    },
    open: true,
    port: 8080,
    compress: true,
    hot: true, //if we change smth in dist - take place autoreload a webserver
  },

});
